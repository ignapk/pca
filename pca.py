#!/usr/bin/env python3

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='pca.py',
                                     description='Zanalizuj główne składowe sygnału',
                                     epilog='example:\n  ./pca.py file.csv\n  ./pca.py --start 2 --end 5 --step 2 --delim . --channels 1 2 --comps 2 --out wynik.csv file.csv',
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('file',
                        help='Plik .csv z danymi sygnału')
    parser.add_argument('--start', type=int,
                        help='Od której próbki zacząć, numeracja od zera, domyślnie pierwsza, np. --start 2 zacznie od 3 próbki')
    parser.add_argument('--end', type=int,
                        help='Na której próbce zakończyć, numeracja od zera, domyślnie ostatnia, np. --end 5 skończy na 6 próbce')
    parser.add_argument('--step', type=int,
                        help='Co ile próbek brać, domyślnie 1, np --step 2 będzie brało co 2 próbki')
    parser.add_argument('--delim',
                        help='Separator kanałów sygnału w podanym pliku, domyślnie ;, np. --delim .')
    parser.add_argument('--channels', nargs='*', type=int,
                        help='Wybierz kanały które brać pod uwagę, domyślnie wszystkie, np. --channels 1 2 pominie pierwszy kanał (przydatne jeśli w pierwszym kanale zapisany jest czas)')
    parser.add_argument('--comps', type=int,
                        help='Liczba głównych komponentów do odnalezienia, domyślnie tyle samo ile kanałów, np. --comps 2 odnajdzie 2 komponenty')
    parser.add_argument('--out',
                        help='Plik wyjściowy .csv z danymi komponentów, domyślnie out.csv, np. --out wynik.csv')
    args = parser.parse_args()

    # Read from the csv
    df = pd.read_csv (args.file,
                      sep=args.delim if args.delim else ';',
                      header=None)

    # Choose interesting samples and disregard the rest
    start = args.start if args.start else 0
    end = args.end if args.end else len(df)
    step = args.step if args.step else 1
    df = df.iloc[list(range(start, end, step))]

    # Choose interesting channels and disregard the rest
    channels = args.channels if args.channels else df.columns
    df = df[channels]

    # Normalize data
    #df = StandardScaler().fit_transform(df)
    #df = pd.DataFrame(df, columns = [1, 2])

    # Prepare entire figure
    fig, axs = plt.subplots(1, 2)

    # Prepare oscylogram of each channel of the signal
    for channel in channels:
        axs[0].plot(range(len(df)), df[channel], label=f'X{channel + 1}')
    axs[0].set_title('Oscylogram sygnału wielokanałowego')
    axs[0].set_xlabel('nr próbki')
    axs[0].set_ylabel('amplituda')
    axs[0].legend()

    # Analyse principal components in specified number
    pca = PCA(n_components=args.comps if args.comps else len(channels))
    principal_components = pca.fit_transform(df)
    pca_df = pd.DataFrame(principal_components)

    # Prepare oscylograms of each principal component of the signal
    for channel in range(len(pca_df.columns)):
        axs[1].plot(range(len(pca_df)), pca_df[channel], label=f'PC{channel + 1}')
    axs[1].set_title('Oscylogram głównych komponentów')
    axs[1].set_xlabel('nr próbki')
    axs[1].set_ylabel('amplituda')
    axs[1].legend()

    # Show entire figure
    fig.set_figwidth(10)
    plt.show()

    # Write components to .csv
    pca_df.to_csv(args.out if args.out else 'out.csv',
              sep=args.delim if args.delim else ';',
              index=False, header=False)
