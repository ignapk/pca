# Principal Component Analysis

Program zaliczeniowy

## Użycie

```
usage: pca.py [-h] [--start START] [--end END] [--step STEP] [--delim DELIM] [--channels [CHANNELS ...]] [--comps COMPS] [--out OUT] file

Zanalizuj główne składowe sygnału

positional arguments:
  file                  Plik .csv z danymi sygnału

options:
  -h, --help            show this help message and exit
  --start START         Od której próbki zacząć, numeracja od zera, domyślnie pierwsza, np. --start 2 zacznie od 3 próbki
  --end END             Na której próbce zakończyć, numeracja od zera, domyślnie ostatnia, np. --end 5 skończy na 6 próbce
  --step STEP           Co ile próbek brać, domyślnie 1, np --step 2 będzie brało co 2 próbki
  --delim DELIM         Separator kanałów sygnału w podanym pliku, domyślnie ;, np. --delim .
  --channels [CHANNELS ...]
                        Wybierz kanały które brać pod uwagę, domyślnie wszystkie, np. --channels 1 2 pominie pierwszy kanał (przydatne jeśli w pierwszym kanale zapisany jest czas)
  --comps COMPS         Liczba głównych komponentów do odnalezienia, domyślnie tyle samo ile kanałów, np. --comps 2 odnajdzie 2 komponenty
  --out OUT             Plik wyjściowy .csv z danymi komponentów, domyślnie out.csv, np. --out wynik.csv

example:
  ./pca.py file.csv
  ./pca.py --start 2 --end 5 --step 2 --delim . --channels 1 2 --comps 2 --out wynik.csv file.csv
```
